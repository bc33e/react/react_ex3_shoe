import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { data_shoes } from "./data_shoe";
import ItemShoe from "./ItemShoe";

export default class Shoe extends Component {
  state = {
    shoe: data_shoes,
    cart: [],
  };

  renderContentShoe = () => {
    return this.state.shoe.map((item) => {
      return <ItemShoe handleAddCart={this.handleAddCart} data={item} />;
    });
  };

  handleAddCart = (shoe) => {
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });

    let cloneCart = [...this.state.cart];

    //Khi chưa có SP trong giỏ
    if (index == -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].quantity++;
    }
    this.setState({ cart: cloneCart });
  };

  handleChangeQuantity = (shoeId, value) => {
    let index = this.state.cart.findIndex((shoe) => {
      return shoe.id == shoeId;
    });
    //Khi số lượng nhỏ hơn 1
    if (index == -1) return;

    //khi tăng giảm số lượng
    let cloneCart = [...this.state.cart];
    cloneCart[index].quantity = cloneCart[index].quantity + value;

    cloneCart[index].quantity == 0 && cloneCart.splice(index, 1);

    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div className="container py-5">
        {this.state.cart.length > 0 && (
          <CartShoe
            handleChangeQuantity={this.handleChangeQuantity}
            cart={this.state.cart}
          />
        )}
        <div className="row">{this.renderContentShoe()}</div>
      </div>
    );
  }
}
