import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name } = this.props.data;
    return (
      <div className="col-3 mt-5">
        <div
          className="card h-100 text-white bg-secondary "
          style={{ width: "100%" }}
        >
          <img className="w-100 card-img-top" src={image} alt="" />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <a
              onClick={() => {
                this.props.handleAddCart(this.props.data);
              }}
              href="#"
              className="btn btn-primary"
            >
              Add to cart
            </a>
          </div>
        </div>
      </div>
    );
  }
}
